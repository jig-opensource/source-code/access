Option Compare Database
Option Explicit

' Tabellen-Tools
' ***************************************************************************

' Extras > Verweise
'   Microsoft Access 16.0 Object Library
'   Microsoft ActiveX Data Objects Recordset 6.0 Library
'   Microsoft ActiveX Data Objects 6.1 Library
'   Microsoft Office 16.0 Access database engine Object Library
'


' ===========================================================================
'  NAME: TableExist
'  DESC: pr�ft ob die Tabelle mit dem angegebenen Namen existiert
'  DESC: check the existing of the table by TableName
' http://www.activevb.de/tipps/vb6tipps/tipp0580.html
' ===========================================================================
Public Function gfkAccTblTableExist(ByVal TableName As String, ByVal ADOConnection As ADODB.Connection) As Boolean
  Dim sName As String
  Dim cat As New ADOX.Catalog

  Set cat.ActiveConnection = ADOConnection     ' Set the catalogs connection

  On Error Resume Next
  Err.Clear                                    ' Clear Errors
  sName = cat.Tables(TableName).Name           ' produce an error, if table does not exist

  gfkAccTblTableExist = (Err = 0)                       ' check on Error = 0
  cat.ActiveConnection.Close
  Set cat.ActiveConnection = Nothing
End Function


' Gibt alle Felder aller Tabellen zur�ck
' ?gfkAccTblListAllTableFields(False)
' ?gfkAccTblListAllTableFields("tAnlage")
Function gfkAccTblListAllTableFields(ByVal IncludeSystemTables As Boolean)
  Dim MyDB As Database
  Dim table As DAO.TableDef
  Dim Feld As DAO.Field
  Set MyDB = DBEngine.Workspaces(0).Databases(0)
  
  With MyDB
     Debug.Print .TableDefs.count & " TableDefs in " & .Name
  
     ' Enumeration der TableDefs-Auflistung.
     For Each table In .TableDefs
        Debug.Print "  " & table.Name
        If (table.Attributes And dbSystemObject) And IncludeSystemTables = False Then
          Debug.Print "  ** SystemTable!"
        Else
          For Each Feld In table.Fields
            Debug.Print "    " & Feld.Name
          Next Feld
        End If
     Next table
  
     .Close
  End With
  Set MyDB = Nothing
End Function


' ?gfkAccTblGetTableFields_Test()
Function gfkAccTblGetTableFields_Test()
  res = gfkAccTblGetTableFields("tAnlage")
  Stop
End Function


' ?gfkAccTblGetTableFields("tAnlage")
Function gfkAccTblGetTableFields(ByVal TableName As String)

  Dim db As DAO.Database
  Dim tdf As DAO.TableDef
  Dim fld As Field
  
  Set db = CurrentDb
  Set tdf = db.TableDefs(TableName)
  
  Dim asTblFields() As Variant
  
  For Each fld In tdf.Fields
    ' Debug.Print fld.Properties("Description")
    ' Debug.Print "    " & fld.Name
    Call AddElementToArray(asTblFields, fld.Name)
  Next fld
  
  gfkAccTblGetTableFields = asTblFields
  
  db.Close
  Set db = Nothing

End Function


Function gfkAccTblGetTableFields_001(ByVal TableName As String, Optional ByVal init As Boolean) As String
  'Stop
  Static Initialized As Boolean
  Static Active As Boolean
  If Not IsNull(init) Then
    If init = True Then
      Initialized = False
      Active = False
    End If
  End If
  Static MyDB
  If IsNull(MyDB) Or Not Initialized Then
    Set MyDB = DBEngine.Workspaces(0).Databases(0)
    Initialized = True
  End If
  
  Static rstEmployees
  Static Counter As Integer
  If Not Active Then
    If IsNull(TableName) Or TableName = "" Then
      Exit Function
    Else
      Counter = 0
      Set rstEmployees = MyDB.OpenRecordset(TableName)
      If Counter < rstEmployees.Fields.count - 1 Then
        Active = True
        gfkAccTblGetTableFields_001 = rstEmployees.Fields(Counter).Name
        Counter = Counter + 1
      Else
        Active = False
        gfkAccTblGetTableFields_001 = ""
        rstEmployees.Close
      End If
    End If
  Else
    ' we are already active
    ' print next Field
    If Counter < rstEmployees.Fields.count Then
      gfkAccTblGetTableFields_001 = rstEmployees.Fields(Counter).Name
      Counter = Counter + 1
    Else
      Active = False
      gfkAccTblGetTableFields_001 = ""
      rstEmployees.Close
    End If
  End If
  ' GetTableFields = rstEmployees.Fields
  rstEmployees.Close
  Set rstEmployees = Nothing
  Set MyDB = Nothing
End Function


' Sucht <SuchFeld> in allen Felder aller Tabellen
' Beispiele, die beide Tabellen finden:
' ?FindTableField("Fund",False)
' ?FindTableField("RefTo_FundID",False)
Function gfkAccTblFindTableField(ByVal SuchFeld As String, ByVal IncludeSystemTables As Boolean)
  Dim MyDB As Database
  Dim table As TableDef
  Dim Feld As Field
  Set MyDB = DBEngine.Workspaces(0).Databases(0)
  Dim Tabelle As String
  Dim found As Boolean
  found = False
  
  With MyDB
     ' Debug.Print .TableDefs.Count & " TableDefs in " & .Name
  
     ' Enumeration der TableDefs-Auflistung.
     For Each table In .TableDefs
        Tabelle = table.Name
        ' Debug.Print "  " & table.Name
        If (table.Attributes And dbSystemObject) And IncludeSystemTables = False Then
          ' Debug.Print "  ** SystemTable!"
        Else
          For Each Feld In table.Fields
          
            ' Suche
            If InStr(1, Feld.Name, SuchFeld) > 0 Then
              Debug.Print Tabelle & ":" & Feld.Name
              found = True
            End If
          Next Feld
        End If
     Next table
  
     .Close
  End With
  Set MyDB = Nothing
  
  If found = False Then
    Debug.Print "Nicht gefunden: " & SuchFeld
  End If
End Function


Function gfkAccTblTabledefs_Test()
  Dim MyDB As Database
  Dim rstEmployees As Recordset
  Dim intloop As Long
  Dim prpLoop As Property
  Set MyDB = DBEngine.Workspaces(0).Databases(0)
  Set rstEmployees = MyDB.OpenRecordset("_config")
  Debug.Print rstEmployees.Fields.count
  
  ' print Table-Fields
  With rstEmployees
    For intloop = 0 To .Fields.count - 1
      Debug.Print "  " & .Fields(intloop).Name
    Next intloop
  End With
  
  'print Table properties
  For Each prpLoop In rstEmployees.Properties
    On Error Resume Next
    Debug.Print "  " & prpLoop.Name & " = " & prpLoop.Value
    On Error GoTo 0
  Next prpLoop
  rstEmployees.Close
  Set rstEmployees = Nothing
  Set MyDB = Nothing
End Function

Function gfkAccTblTabledefs_Test1()
  Dim MyDB As Database
  Dim tdfLoop As TableDef
  Dim tdfNew As TableDef
  Dim prpLoop As Property
  Set MyDB = DBEngine.Workspaces(0).Databases(0)
  'Dim tdfNew As TableDef
  'Dim tdfLoop As TableDef
  'Dim prpLoop As Property
  
  With MyDB
     Debug.Print .TableDefs.count & _
        " TableDefs in " & .Name
  
     ' Enumeration der TableDefs-Auflistung.
     For Each tdfLoop In .TableDefs
        Debug.Print "  " & tdfLoop.Name
        If tdfLoop.Name = "_config" Then
          Set tdfNew = tdfLoop
        End If
     Next tdfLoop
  
     With tdfNew
        Stop
        Debug.Print "Eigenschaften von " & .Name
  
        ' Enumeration der Properties-Auflistung jedes
        ' neuen TableDef-Objekts; nur die
        ' Eigenschaften mit nicht-leeren Werten
        ' ausgeben.
        For Each prpLoop In .Properties
           Debug.Print "  " & prpLoop.Name & " - " & _
              IIf(prpLoop = "", "[leer]", prpLoop)
        Next prpLoop
  
     End With
  
     .Close
  End With
  Set MyDB = Nothing
End Function

